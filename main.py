#!/usr/bin/env pybricks-micropython

from time import sleep
from pybricks import ev3brick as brick
from pybricks.ev3devices import (Motor, TouchSensor, ColorSensor,
                                 InfraredSensor, UltrasonicSensor, GyroSensor)
from pybricks.parameters import (Port, Stop, Direction, Button, Color,
                                 SoundFile, ImageFile, Align)
from pybricks.tools import print, wait, StopWatch
from pybricks.robotics import DriveBase
#global non state automa variable
left_motor = Motor(Port.A)
right_motor = Motor(Port.D)

left_sensor_color =  ColorSensor(Port.S1)
right_sensor_color = ColorSensor(Port.S3)
middle_sensor_color = ColorSensor(Port.S2)

black = middle_sensor_color.rgb()
white = left_sensor_color.rgb()
ambient = middle_sensor_color.reflection()

left_color_value = ""
right_color_value = ""
middle_color_value = ""
middle_ambient_value = 0


#function isColor(tuple)
def isColor(rgb):
    #check if it is black with 25% of tollerance(cartesian graph integral of maximum reveleable (pi/4))
    global black,white
    #tolerance is 1/x when x is your number(don't change it if you don't wanna die)
    tolerance = 2
    if ( (rgb[0] >= (black[0] - (black[0])/tolerance)) and (rgb[0] <= (black[0] + (black[0])/tolerance)) ) and ( (rgb[1] >= (black[1] - (black[1])/tolerance)) and (rgb[1] <= (black[1] + (black[1])/tolerance)) ):
            return "black"
    elif rgb[1] > (rgb[0] + rgb[0]/2):
        return "green"
    else: 
        return "white"
    
def fetch():
    global left_sensor_color,right_sensor_color,middle_sensor_color,left_color_value,right_color_value,middle_color_value,middle_ambient_value
    left_values =  left_sensor_color.rgb()
    right_values = right_sensor_color.rgb()
    middle_values = middle_sensor_color.rgb()
    middle_ambient_value = middle_sensor_color.reflection()
    left_color_value = isColor(left_values)
    right_color_value = isColor(right_values)
    middle_color_value = isColor(middle_values)
    

def print_fetch():
    global left_color_value,right_color_value,middle_color_value,middle_ambient_value
    brick.display.text("left: " + str(left_color_value),(10,20))
    brick.display.text("right: " + str(right_color_value),(10,40))
    brick.display.text("middle: " + str(middle_color_value),(10,60))
    brick.display.text("ambient: " + str(middle_ambient_value),(10,80))

#definition of all states
def pid_state():
    target = 150
    kp = 40000
    kd = 200
    ki = 100
    e1 = 0
    e2 = 0
    m1_speed = 0
    m2_speed = 0
    while left_color_value == "white" and right_color_value == "white" and middle_color_value == "black":
        fetch()
        print_fetch()
        e1_error = target - e1
        e2_error = target - e2
        m1_speed += e1_error * kp
        m2_speed += e2_error * kp
        m1_speed = max(min(1,m1_speed), 0)
        m2_speed = max(min(1,m2_speed), 0)
        left_motor.run(m1_speed*10)
        right_motor.run(m1_speed*10)
        e1 = 0
        e2 = 0
        e1_prev_error = 0
        e2_prev_error = 0
        m1_speed += (e1_error*kp) + (e1_prev_error * kd)
        m2_speed += (e2_error*kp) + (e2_prev_error * kd)
        e1_prev_error = e1_error
        e2_prev_error = e2_error
        e1_sum_error = 0
        e2_sum_error = 0
        m1_speed += (e1_error*kp)+(e1_prev_error*kd) + (e1_sum_error*ki)
        m2_speed += (e2_error*kp)+(e2_prev_error*kd) + (e2_sum_error*ki)
        e1_sum_error += e1_error
        e2_sum_error += e2_error
        
       
        
        


    

#definition of all variable we need for automa
memory = []
adiacenty_list = [
    [-1] #state 0  PID algorithm all states can be enter in it
]

while True:
    fetch()
    print_fetch()

    #example of state 0 enter and check previous states
    if left_color_value == "white" and right_color_value == "white" and middle_color_value == "black":
        memory.append(0)
        pid_state()


    sleep(0.5)
    brick.display.clear()
